package com.deecoders.demojson;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView clistview;
    ArrayList<Databean> companylist,comlist;
    DataAdapter companyadapter;
    ProgressDialog pd;
    String banner_img,submit_text,display_name,description;

    void init(){
        clistview=(ListView)findViewById(R.id.listview);
        pd=new ProgressDialog(this);
        pd.setMessage("Please Wait...");
        pd.setCancelable(false);
        pd.show();
        handler.sendEmptyMessage(100);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what==100){
                companylist=new ArrayList<>();
                String url="https://www.reddit.com/reddits.json";
                JsonObjectRequest jsonObjectRequest=new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject jsonObject) {
                        try {
                            JSONObject data=jsonObject.getJSONObject("data");
                            JSONArray children=data.getJSONArray("children");
                            for(int i=0;i<children.length();i++){
                                JSONObject jobj=children.getJSONObject(i);
                                JSONObject newdata=jobj.getJSONObject("data");
                                banner_img=newdata.getString("banner_img");
                                submit_text=newdata.getString("submit_text");
                                display_name=newdata.getString("display_name");
                                description=newdata.getString("description");
                                companylist.add(new Databean(banner_img,submit_text,display_name,description));
                            }

                            companyadapter=new DataAdapter(MainActivity.this,R.layout.companylay,companylist);
                            clistview.setAdapter(companyadapter);
                            clistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    Databean db=companylist.get(i);
                                    Intent fullview=new Intent(MainActivity.this,Fullview.class);
                                    fullview.putExtra("img",banner_img);
                                    fullview.putExtra("desc",description);
                                    startActivity(fullview);
                                }
                            });
                            pd.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i("jokeofday",jsonObject.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Log.i("jokeerror",volleyError.toString());
                    }
                });
                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(10000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                Volley.newRequestQueue(getApplicationContext()).add(jsonObjectRequest);

            }
        }
    };
}
