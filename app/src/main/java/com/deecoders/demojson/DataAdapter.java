package com.deecoders.demojson;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

import java.util.List;

/**
 * Created by Deepak Bansal on 10/2/2016.
 */
public class DataAdapter extends ArrayAdapter {

    Context context;
    List<Databean> companylist;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    public DataAdapter(Context context, int resource, List<Databean> objects) {
        super(context, resource, objects);
        this.context=context;
        this.companylist=objects;
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });

    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view=null;
        LayoutInflater layoutInflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view=layoutInflater.inflate(R.layout.companylay,parent,false);

        Databean cb=companylist.get(position);

        TextView txtcompanyname=(TextView)view.findViewById(R.id.fname);
        txtcompanyname.setText(cb.getDisplay_name());
        NetworkImageView img=(NetworkImageView)view.findViewById(R.id.listicon);
        img.setImageUrl(cb.getBanner_img(),mImageLoader);

        return view;
    }
}
