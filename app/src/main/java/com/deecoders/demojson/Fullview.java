package com.deecoders.demojson;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.LruCache;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;

public class Fullview extends AppCompatActivity {

    NetworkImageView imgview;
    TextView desc;
    String img,des;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    void  init(){
        imgview=(NetworkImageView)findViewById(R.id.listicon);
        desc=(TextView)findViewById(R.id.desc);
        mRequestQueue = Volley.newRequestQueue(this);
        mImageLoader = new ImageLoader(mRequestQueue, new ImageLoader.ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<String, Bitmap>(10);

            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }

            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
        });
        imgview.setImageUrl(img,mImageLoader);
        desc.setText(des);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullview);

        Intent rcv=getIntent();
        img=rcv.getStringExtra("img");
        des=rcv.getStringExtra("desc");
        init();
    }
}
