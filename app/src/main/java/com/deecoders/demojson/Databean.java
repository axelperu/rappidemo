package com.deecoders.demojson;

/**
 * Created by Deepak Bansal on 10/2/2016.
 */
public class Databean {

    String banner_img;
    String submit_text;
    String display_name;

    public Databean(String banner_img, String submit_text, String display_name, String description) {
        this.banner_img = banner_img;
        this.submit_text = submit_text;
        this.display_name = display_name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBanner_img() {
        return banner_img;
    }

    public void setBanner_img(String banner_img) {
        this.banner_img = banner_img;
    }

    public String getSubmit_text() {
        return submit_text;
    }

    public void setSubmit_text(String submit_text) {
        this.submit_text = submit_text;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    String description;
}
